# whats cooking
This Kaggle competition asks us to predict the category of a dish's cuisine given a list of its ingredients. The dataset is provided by Yummly.
Source : https://www.kaggle.com/c/whats-cooking-kernels-only
In the dataset, we include the recipe id, the type of cuisine, and the list of ingredients of each recipe (of variable length). The data is stored in JSON format. 
An Example of Train data
{
 "id": 24717,
 "cuisine": "indian",
 "ingredients": [
     "tumeric",
     "vegetable stock",
     "tomatoes",
     "garam masala",
     "naan",
     "red lentils",
     "red chili peppers",
     "onions",
     "spinach",
     "sweet potatoes"
 ]
 }
This notebook provides a step-by-step analysis and solution to the given problem. It can also serve as a great starting point for learning how to explore, manipulate, transform and learn from textual data. It is divided into three main sections:

Exploratory Analysis - as a first step, we explore the main characteristics of the data with the help of Plotly vizualizations

Text Processing - here we apply some basic text processing techniques in order to clean and prepare the data for model development

Feature Engineering & Data Modeling - in this section we extract features from data and build a predictive model of the cuisine.


I had done some data visualization Techniques using Pyplot and made some conclusion
Tried different Featurization and modeling .came to conclusion that RBF SVM performs well to this data
